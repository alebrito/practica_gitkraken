/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author serafin
 */
public class Municipio {
    
    private int id_municipio;
    
    private String nombre_municipio;
    
    private Departamento fk_departamento;

    public int getId_municipio() {
        return id_municipio;
    }

    public void setId_municipio(int id_municipio) {
        this.id_municipio = id_municipio;
    }

    public String getNombre_municipio() {
        return nombre_municipio;
    }

    public void setNombre_municipio(String nombre_municipio) {
        this.nombre_municipio = nombre_municipio;
    }

    public Departamento getFk_departamento() {
        return fk_departamento;
    }

    public void setFk_departamento(Departamento fk_departamento) {
        this.fk_departamento = fk_departamento;
    }
    
    
    
}
