/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Josué
 */
public class Enlace {
    
    private int id_enlace;
    private Cliente fk_cliente;
    private Cuenta fk_cuenta;

    public int getId_enlace() {
        return id_enlace;
    }

    public void setId_enlace(int id_enlace) {
        this.id_enlace = id_enlace;
    }

    public Cliente getFk_cliente() {
        return fk_cliente;
    }

    public void setFk_cliente(Cliente fk_cliente) {
        this.fk_cliente = fk_cliente;
    }

    public Cuenta getFk_cuenta() {
        return fk_cuenta;
    }

    public void setFk_cuenta(Cuenta fk_cuenta) {
        this.fk_cuenta = fk_cuenta;
    }
    
    
}
