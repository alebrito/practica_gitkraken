/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author serafin
 */
public class Departamento {

    private int id_departamento;

    private String nombre_depa;

    public int getId_departamento() {
        return id_departamento;
    }

    public void setId_departamento(int id_departamento) {
        this.id_departamento = id_departamento;
    }

    public String getNombre_depa() {
        return nombre_depa;
    }

    public void setNombre_depa(String nombre_depa) {
        this.nombre_depa = nombre_depa;
    }

}
