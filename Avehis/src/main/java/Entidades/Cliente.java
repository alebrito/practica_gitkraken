/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author serafin
 */
public class Cliente {

    private int id_cliente;

    private String nombre;

    private String direccion;

    private Municipio fk_municipio;

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Municipio getFk_municipio() {
        return fk_municipio;
    }

    public void setFk_municipio(Municipio fk_municipio) {
        this.fk_municipio = fk_municipio;
    }

}
